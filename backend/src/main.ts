import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.setGlobalPrefix('api');
  
  const s3URL = "https://my-k8s-app.s3.eu-central-1.amazonaws.com/index.html"
  console.log('S3 Bucket URL:', s3URL);
  
  await app.listen(3000);
}
bootstrap();
